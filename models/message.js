var mongoose = require('mongoose')
, Schema = mongoose.Schema

/////////********  mongoose model
var messages = mongoose.model('messages', {
 
  chat_id: {type: Schema.Types.ObjectId,ref:'user'},
  message: { type: String },
  origin: {type: Schema.Types.ObjectId,ref:'user'},
  
 
  isDeleted:{type: Boolean, default: false },
  
});

module.exports = { messages }
