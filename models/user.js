var mongoose = require('mongoose')

/////////********  mongoose model
var Users = mongoose.model('users', {
 
  username: {type: String,unique: true,minlength: 3,trim: true},
  DOB: {type: Date},
  password: {type: String},
  mobile: { type: Number,trim: true },
  role: { type: String, ENUM: ['ADMIN', 'USER'] },
  status: {type: String,ENUM:['ACTIVE','INACTIVE','BLOCKED']},
  email: { type: String, default: "",trim: true },
  currentLocation: { type: String, default: "" },
  location: { type: String, default: "" },
  displayName: { type: String, default: "" ,trim: true},
  displayPic: { type: String, default: "" },
  pic: { type: String, default: "" },
  About: { type: String, default: "" },
  isDeleted:{type: Boolean, default: false },
  lastOnlineTime: {type: Date,default: Date.now()}
});

module.exports = { Users }
