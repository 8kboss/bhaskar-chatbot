var mongoose = require('mongoose')
, Schema = mongoose.Schema
/////////********  mongoose model
var Chats = mongoose.model('chats', {
 
  origin: {type: Schema.Types.ObjectId,ref:'user'},
  destination: {type: Schema.Types.ObjectId,ref:'user'},
  isDeleted:{type: Boolean, default: false },
  
});

module.exports = { Chats }
